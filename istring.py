# i-string implementation ##############################################
import sys
import _string

def _default_value_lookup(expr_txt, format_spec, conversion, value_map):
    # by default, raise a KeyError if the expression wasn't in the expressions
    #  we originally parsed
    return value_map[expr_txt], format_spec, conversion

def _default_value_conversion(value, format_spec, conversion):
    return str(value)

def _str_value_conversion(value, format_spec, conversion):
    if conversion == 's':
        value = str(value)
    elif conversion == 'r':
        value = repr(value)
    elif conversion == 'a':
        value = ascii(value)
    return value.__format__(format_spec)

def _build_expression_map(s, _level=2):
    # evaluate the expressions now, and remember them. Returns a dict
    #  mapping the expression text to its evaluated value.
    # _level is an implementation hack, and should never be set by
    #  user code. it's used to get f() and i() to work. It won't be
    #  needed when we modify the compiler to compute this mapping.
    result = {}
    locals = sys._getframe(_level).f_locals
    globals = sys._getframe(_level).f_globals
    for literal, expr_txt, format_spec, conversion in \
            _string.formatter_parser(s):
        # Throw away the literals. We'll re-get them later in
        #  join() or parts().
        if expr_txt is not None:
            value = eval(expr_txt, locals, globals)
            result[expr_txt] = value
    return result

class InterpolationTemplate:
    __slots__ = ('template_string', 'value_map')

    sentinel = object()

    def __new__(cls, template_string, value_map):
        # template_string is the original string from which the
        #  expressions were extracted. value_map maps the expressions'
        #  text to their matching values. Normally, template_string
        #  will be used when evaluating this template object, although
        #  it's possible to override it with another string (say, for
        #  i18n/l10n).
        self = super().__new__(cls)
        self.template_string = template_string
        self.value_map = value_map
        return self

    def __str__(self):
        return self.join(value_conversion=_str_value_conversion)

    def join(self, template_string=None, *, value_lookup=None, value_conversion=None):
        if value_conversion is None:
            value_conversion = _default_value_conversion
        # Join together the literals and the result of the
        #  expressions.  If template_string is None, then parse the
        #  original template_string that was passed in to
        #  __new__. Otherwise, use the value template_string passed in
        #  here.  Each value is found by calling value_lookup. This
        #  allows for 'safe substitution'. value_lookup can also
        #  modify the format_spec and conversion char.  Each value is
        #  replaced by value_conversion(value, format_spec,
        #  conversion). The default value_conversion uses str(value)
        #  and ignores the format_spec and conversion.
        result = []
        for literal, value, format_spec, conversion in \
                self.parts(template_string, value_lookup=value_lookup):
            result.append(literal)
            # If we have an expression, use it. We won't have one at
            #  the end of the string (it always ends with just a
            #  literal).
            if value is not self.sentinel:
                result.append(value_conversion(value, format_spec, conversion))
        return ''.join(result)

    # Yields tuples of (literal, value, format_spec, conversion).
    # Because we're yielding the values of the expressions, we need
    #  some way of saying we're at the end of the expression. This is
    #  currently being done with having the value be i.sentinel. This
    #  is not a great interface, but we can't use None because a value
    #  could legitimately be None. We either need a sentinel value, or
    #  a flag that says "there's no value here".
    # If template_string is None, then use the original string passed
    #  in at object creation time. Otherwise, use
    #  template_string. This allows for a seperate string to be passed
    #  in, mostly useful in an i18n/l10n translation scenario.
    # value_lookup is called with (expr_txt, format_spec, conversion,
    #  value_map). It must return (value, format_spec,
    #  conversion). expr_txt is the text of the expression, as
    #  extracted from the original template string used at creation
    #  time. format_spec and conversion are extracted from the
    #  original template string, or the template_string parameterq if
    #  provided. value_map is the dict mapping each expr_txt to the value
    #  calculated at i-string creation time. If value_lookup is not
    #  supplied or is None, then the value lookup is calculated by
    #  value_map[expr_txt], and format_spec and conversion are unchanged.
    def parts(self, template_string=None, *, value_lookup=None):
        if template_string is None:
            template_string = self.template_string
        if value_lookup is None:
            value_lookup = _default_value_lookup
        for literal, expr_txt, format_spec, conversion in \
                _string.formatter_parser(template_string):
            if expr_txt is None:
                value = self.sentinel
            else:
                value, format_spec, conversion = value_lookup(expr_txt, format_spec, conversion, self.value_map)
            yield literal, value, format_spec, conversion

# i-string implementation ##############################################
def i(s):
    return InterpolationTemplate(s, _build_expression_map(s))

# f-string implementation ##############################################
def f(s):
    return str(InterpolationTemplate(s, _build_expression_map(s)))
