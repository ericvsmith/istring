A repository for testing out ideas with Python "i-strings". See the
discussion at
https://mail.python.org/pipermail/python-ideas/2015-August/035469.html
for background information.

What PEP 498 calls an "f-string", and would designate as f"", here is
implemented as f(""). The delayed rendering version of these,
"i-strings", are implemented as i(""). I think PEP 501 is converging
on i-strings as implemented here.

This project is for experimenting with ways of using i-strings in what
PEP 501 calls "custom interpolators".  As part of that, it's a testbed
for determining a useful API for i-strings and the proposed
types.InterpolationTemplate object.

This project uses sys._getframe() to find the locals and globals used
where the f- or i-string appears in the source code, and then invokes
eval(). If accepted, the approach would be to modify the Python
compiler, and there would be no need to use sys._getframe or eval.

To test::

 python3 simple.py
 python3 i18n.py
 python3 log.py
 python3 regex.py
 python3 listify.py

Eric.
