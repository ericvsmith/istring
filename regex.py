from istring import i

# re code ##############################################################
import re
def to_re(istring):
    def callback(value, format_spec, conversion):
        if format_spec == 'raw':
            return str(value)
        else:
            return re.escape(str(value))

    s = istring.join(value_conversion=callback)
    return re.compile(s)


# test code ############################################################

if __name__ == '__main__':
    import unittest

    class TestCase(unittest.TestCase):
        def test(self):
            delimiter = '+'
            trailing_re = r'\S*'
            regex = to_re(i(r'{delimiter}\d+{delimiter}{trailing_re:raw}'))

            self.assertRegex('+123+', regex)
            self.assertRegex('+123+  ', regex)

    unittest.main()
