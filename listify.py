from istring import i

# convert an i-string to a list of literals and expressions. strip the literals
#  and values. ignore empty literals

# this is really just here as an example of code that converts an i-string to
#  something that's not a list

def to_list(istring, s=None, value_lookup=None):
    result = []
    for literal, value, format_spec, conversion in istring.parts(s, value_lookup=value_lookup):
        literal = literal.strip()
        if literal:
            result.append(literal)
        if value is not istring.sentinel:
            if value:
                result.append(value)
    return result


if __name__ == '__main__':
    import unittest

    class TestCase(unittest.TestCase):
        def test(self):
            expr1 = 42
            expr2 = 'text'

            istr = i('x {expr1*2} {expr2} y')
            self.assertEqual(to_list(istr), ['x', 84, 'text', 'y'])

            # make sure that an expression passes through as a
            #  reference to the original object
            expr3 = object()
            self.assertIs(to_list(i('{expr3}'))[0], expr3)

    unittest.main()
