from istring import i, f

def func(value):
    return i('called func with "{value:10}"')

if __name__ == '__main__':
    import sys
    import unittest
    import datetime

    class TestCase(unittest.TestCase):
        def test_format_spec(self):
            self.assertEqual(f('{datetime.date(1991, 10, 12):%A}'), 'Saturday')
            self.assertEqual(str(i('{datetime.date(1991, 10, 12):%A}')), 'Saturday')

        def test_f(self):

            name = 'Eric'
            dog = 'Misty'
            self.assertEqual(f('My name is {name}, my dog is {dog}'), 'My name is Eric, my dog is Misty')
            self.assertEqual(f('{func(42)}'), 'called func with "        42"')

            for x in range(10):
                self.assertEqual(f('{x:2}'), ' ' + str(x))

            self.assertEqual(f(''), '')
            with self.assertRaises(SyntaxError):
                f('{}')

        def test_i(self):
            # need to add some test to make sure that the i-string isn't being evaluated early
            x = i('{sys.version.upper()}')
            self.assertTrue(str(x).isupper())

            # changing a mutable value doesn't affect the i-string
            n = 0
            x = i('{n}')
            self.assertEqual(str(x), '0')
            n = 1
            self.assertEqual(str(x), '0')

            # but a mutable value will
            l = [1]
            x = i('{l}')
            self.assertEqual(str(x), '[1]')
            l[0] = 2
            self.assertEqual(str(x), '[2]')
            l = [3]
            self.assertEqual(str(x), '[2]')

    unittest.main()
