from istring import i, f
import _string

def gettext(s):
    # Our complicated string lookup
    if s == 'My name is {name}, my dog is {dog}':
        return 'Mi pero es {dog}, y mi nombre es {name}'
    if s == 'A Bad Translation: {name:<bad format spec>}':
        # this is a bad translation because they've changed the
        #  name of the expression: nombre doesn't exist
        return 'A Bad Translation: {nombre:x}'
    return s

# i18n framework #######################################################

def safe_value_lookup(expr, format_spec, conversion, value_map):
    try:
        return value_map[expr], format_spec, conversion
    except KeyError:
        return '{{{0}}}'.format(expr), '', None

def _(istring):
    # Handle either strings or istrings.
    if isinstance(istring, str):
        return istring
    # do the gettext lookup
    s = gettext(istring.template_string)
    # use the values from our original istring,
    #  but the literals and ordering from our
    #  looked-up string
    return istring.join(s, value_lookup=safe_value_lookup)

def convert_istring_format_to_dollar_format(s):
    result = []
    for literal, expr, format_spec, conversion in \
            _string.formatter_parser(s):
        result.append(literal.replace('$', '$$'))
        if expr:
            if format_spec:
                raise ValueError(f('cannot specify format_spec: {format_spec}'))
            if conversion:
                raise ValueError(f('cannot specify conversion: {conversion}'))
            if not expr.isidentifier():
                raise ValueError(f('{expr} is not an identifier'))
            result.append('${')
            result.append(expr)
            result.append('}')
    return ''.join(result)

########################################################################

if __name__ == '__main__':
    import unittest

    class TestCase(unittest.TestCase):
        def test(self):
            name = 'Eric'
            dog = 'Misty'
            x = i('My name is {name}, my dog is {dog}')
            self.assertEqual(str(x), 'My name is Eric, my dog is Misty')
            self.assertEqual(_(x), 'Mi pero es Misty, y mi nombre es Eric')

            self.assertEqual(_("A normal string"), 'A normal string')
            self.assertEqual(_(i("A Bad Translation: {name:<bad format spec>}")), 'A Bad Translation: {nombre}')

        def test_convert(self):
            self.assertEqual(convert_istring_format_to_dollar_format('{test}'), '${test}')
            self.assertEqual(convert_istring_format_to_dollar_format('{{}}{test}'), '{}${test}')
            self.assertEqual(convert_istring_format_to_dollar_format('${test}'), '$$${test}')
            self.assertEqual(convert_istring_format_to_dollar_format('{test}\n'), '${test}\n')

            with self.assertRaises(ValueError):
                convert_istring_format_to_dollar_format('{a.b}')
            with self.assertRaises(ValueError):
                convert_istring_format_to_dollar_format('{a:3}')
            with self.assertRaises(ValueError):
                convert_istring_format_to_dollar_format('{a!r}')

    unittest.main()
