from istring import i


# logging code #########################################################
class Logger:
    def __init__(self):
        self.output = []

    def log(self, istring):
        self.output.append(istring)

    def __str__(self):
        # delay the string substitutions until now
        return '\n'.join([str(o) for o in self.output])

if __name__ == '__main__':
    import unittest
    import datetime

    class TestCase(unittest.TestCase):
        def test(self):
            now = datetime.datetime(2015, 8, 10, 12, 13, 15)

            log = Logger()
            log.log(i('as of {now:%Y-%m-%d} the value is {400+1:#06x}'))
            log.log(i('test'))

            # XXX: add code to make sure that the expressions haven't been substituted yet
            self.assertEqual(str(log), 'as of 2015-08-10 the value is 0x0191\ntest')

    unittest.main()
